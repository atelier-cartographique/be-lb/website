Site web pour portail geodata.environnement.brussels


# Install

Pour installer à partir d'un clone :

```
pip install path_to_website
```

pour installer depuis l'origine : 

```
pip install git+https://gitlab.com/atelier-cartographique/be-lb/website.git
```

# Config

Ajouter aux settings Django :

```python

INSTALLED_APPS.append('website')

ROOT_WEB_URLS = 'website.urls'

```

