from django.shortcuts import render


def index(request):
    return render(request, 'website/index.html', context=dict())

def index_fr(request):
    return render(request, 'website/index-fr.html', context=dict())

def index_nl(request):
    return render(request, 'website/index-nl.html', context=dict())

def wcag_fr(request):
    return render(request, 'website/wcag-fr.html', context=dict())

def wcag_nl(request):
    return render(request, 'website/wcag-nl.html', context=dict())


