from django.urls import path
from .views import index, index_fr, index_nl, wcag_fr,  wcag_nl 

urlpatterns = [
    path('', index, name='web.index'),
    # path('index-fr.html', index_fr, name='web.index_fr'),
    path('client/view/?lang=fr', index_fr, name='web.index_fr'),
    path('index-nl.html', index_nl, name='web.index_nl'),
    path('wcag-fr.html', wcag_fr, name='web.wcag_fr'),
    path('wcag-nl.html', wcag_nl, name='web.wcag_nl'),
]